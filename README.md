# vue+cordova
    使用cordova插件必须在页面引用cordova.js、cordova_plugin.js
    corodva封装调用方法在common/cordova.js目录下
    页面调用参考view/cordova/index文件
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
