import Vue from 'vue'
import Vuex from 'vuex'
import {getStore,setStore} from '@/common/store'

Vue.use(Vuex);

const state={
	menu_status:false,
	tab_active:'0',
    userInfo:{},
    pickup:{
        pickupLocation:'',    //发件地点
        pickupPerson:'',      //发件人姓名
        pickupPhone:'',       //发件人手机号
        pickupLongitude:'',   //发件地点经度
        pickupLatitude:'',    //发件地点纬度
        startCity:''		//发件人城市
    },
    delivered:{
        deliveredLocation:'', //收件地点
        deliveredPerson:'',   //收件人姓名
        deliveredPhone:'',    //收件人手机号
        deliveredLongitude:'',//收件地点经度
        deliveredLatitude:'', //收件地点纬度
        targetCity:''		//取件人城市
    },
    goods:{
        isBiggds:'',          //是否超大件（00:否01是）
        gdsId:'',             //物品属性id
        gdsName:'',           //物品名称
        gdsPhotos:'',         //物品图片
        deliveryTime:'',      //要求送达日期
        weight:0,             //物品重量
        describe:''           //物品描述

    },
    redenvelope:{
		redenvelopeId:''
    },
    goodsInfo:{
        shippingMethod:'',    //送货方式（00:顺丰送01:紧急送）
        pinkupTime:'',        //要求取货日期

        periodTime:'',        //及时送时间段
        redenvelopeId:'',     //红包id
        gdsNumber:'',         //物品数量
        gdsVolume:'',         //物品体积
        valuationAmount:'',    //保价金额
        redenvelopeName:''	  //红包名称
    }
}

const getters = {

}

const actions = {
	menuAciton(context){
		context.commit('menu');
	}
}

const mutations = {
	menu(state){//这里的state对应着上面这个state
        state.menu_status = state.menu_status?false:true;
        //你还可以在这里执行其他的操作改变state
    },
    userInfo(state,type){
        state.userInfo=type;
        setStore({name:'userInfo',content:type});
    },
    //物品
    goodsInfo(state,data){
        state.goods = data;
    },
    //发件信息
    pickupInfo(state,data){
        state.pickup = data;
        console.log(state,data);
    },
    //收件信息
    deliveredInfo(state,data){
        state.delivered = data;
    },
    //红包
    redenvelopeInfo(state,data){
        state.redenvelope = data;
    }
    //

}

export default new Vuex.Store({
    state,
    actions,
    mutations
})
