import Vue from 'vue'
import App from './App.vue'
import Vant from 'vant';
import router from './router';
import api from '@/api/index'
import store from '@/store'
import http from '@/common/request'
import 'vant/lib/index.css';
import '@/assets/css/common.css';
import cordova from '@/common/cordova';
//import TdFooter from '@/common/custom/footer'

import Clipboard from 'clipboard';  

Vue.use(Vant);



//Vue.component('mt-footer', TdFooter)      //注册footer

Vue.config.productionTip = false
Vue.prototype.$api = api //调用api数据接口地址
//将 axios 改写为 Vue 的原型属性
Vue.prototype.$post = http.post
Vue.prototype.$get = http.get
Vue.prototype.$files = http.files
Vue.prototype.$cordova = cordova


Vue.prototype.$Clipboard =Clipboard
// document.addEventListener('deviceready', function () {
  new Vue({
    router,
    store,
    render: h => h(App),
  }).$mount('#app')
// }, false)
