const _import = require('@/router/_import')

// 作为Main组件的子页面展示并且在左侧菜单显示的路由写在appRouter里
export const appRouter = [
    {
        path: '/',
        redirect: '/cordova'
    },
    {
        path: '/cordova',
        name : '首页',
        component: _import('cordova/index'),
    },

];

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
    // loginRouter,
    // ...otherRouter,
    ...appRouter
];
