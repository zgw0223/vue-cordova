import Vue from 'vue'
import VueRouter from 'vue-router'
// import {routers, otherRouter, appRouter} from './router';
import {routers, appRouter} from './router';

Vue.use(VueRouter)


// 路由配置
const RouterConfig = {
    // mode: 'history',
    routes: routers
};

const routes = new VueRouter(RouterConfig);



// routes.beforeEach((to, from, next) => {
//     console.log("state.token",state.token);
//     //不是登录页面进行判用户是否存在
//     if(to.name !=='login' && !state.token){
//          next({name:"login"});
//     }else{
//          next();
//     }
//     iView.LoadingBar.start();
// })



export default routes;
