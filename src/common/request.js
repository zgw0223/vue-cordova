import axios from 'axios'
// import {Notice, Message} from 'iview';
import store from '@/store'
const qs = require('qs')
// axios基本配置
axios.defaults.timeout = 50000


// http request 拦截器
axios.interceptors.request.use(config => {
      if (store.state.auth_info) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
            config.headers['token'] = `${store.state.auth_info.token}`;
      }
    return config
  },err => {
          // console.log("err",err);
    return Promise.reject(err)
  }
)

// http response 拦截器
axios.interceptors.response.use(response => {
     return response.data
  },error => {
    if (error && error.response) {
        // console.log("error:"+error);
        responseError(error);
    }
    return Promise.reject(error)
})

//错误执行提示
const responseError =function(err){
        switch (err.response.status) {
        case 400:
            // err.message = '400:请求错误'
            break
        case 401:
            // err.message = '401:未授权，请登录'
            break
        case 403:
            // err.message = '503:拒绝访问'
            break
        case 404:
            // err.message = `404:请求地址出错: ${err.response.config.url}`
            break
        case 408:
            // err.message = '408:请求超时'
            break
        case 500:
            // err.message = '500:服务器内部错误'
            break
        case 501:
            err.message = '501:服务未实现'
            break
        case 502:
            err.message = '502:网关错误'
            break
        case 503:
            err.message = '503:服务不可用'
            break
        case 504:
            err.message = '504:网关超时'
            break
        case 505:
            err.message = '505:HTTP版本不受支持'
            break
        default:
        }
        // Notice.error({title: '错误提示',desc: err.message });
}

const http = {
    /**
     * post 请求方法
     * @param url
     * @param data
     * @returns {Promise}
     */
    post (url, data) {
     if (store.state.auth_info) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
            data.token = `${store.state.auth_info.token}`;
        //config.data.token = `${store.state.auth_info.token}`;
    }
        return new Promise((resolve, reject) => {
            axios.post(url, qs.stringify(data)).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    },
    /**
     * get 请求方法
     * @param url
     * @param data
     * @returns {Promise}
     */
    get (url, data) {
        return new Promise((resolve, reject) => {
            axios.get(url, {params:data}).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    },
    /**
     * put 请求方法
     * @param url
     * @param id
     * @param data
     * @returns {Promise}
     */
    put (url, id, data) {
        return new Promise((resolve, reject) => {
            axios.put(url + id, data).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    },
    /**
     * delete 请求方法
     * @param url
     * @param id
     * @returns {Promise}
     */
   delete (url, id) {
        return new Promise((resolve, reject) => {
            axios.delete(url + id).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    },
    /**
     * post 请求方法  请求类型为application/json
     * @param url
     * @param data
     * @returns {Promise}
     */
    json_post (url, data) {
        if (store.state.auth_info) {  // 判断是否存在token，如果存在的话，则每个http header都加上token
            data.token = `${store.state.auth_info.token}`;
        //config.data.token = `${store.state.auth_info.token}`;
        }
        return new Promise((resolve, reject) => {
            axios.post(url, data,{headers:{'Content-Type': 'application/json;charset=UTF-8'}}).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    },
    files (url, params) {
        var data = new FormData();
        for (var key in params){
            data.append(key, params[key]);
        }
        return new Promise((resolve, reject) => {
            axios.post(url, data, {headers: {'Content-Type': 'multipart/form-data'}}).then(response => {
                resolve(response)
            }, err => {
                reject(err)
            })
        })
    }
}


export default http
