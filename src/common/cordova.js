const cordovaFlag = true; // sdk开关
//所有cordova插件都需要在index.html引入cordova.js和cordova_plugins.js
export default {
  //支付宝支付
  //1、安装cordova plugin add cordova-plugin-alipay-v2 --variable APP_ID=[your AppId]
  //如果申请的是老版的支付宝支付需用cordova plugin add https://github.com/chenyuanchn/cordova-plugin-alipay.git --variable PARTNER_ID=[你的商户PID可以在账户中查询] （对于android系统，可以不传PARTNER_ID）
  alipay:function(data,callback_success,callback_error){
    if(cordovaFlag)
      cordova.plugins.alipay.payment(data,			/*Data由程序提供*/
        function success(Data){
          callback_success && callback_success(Data);
        },
        function error(Data){
          callback_error && callback_error(Data)
        }
      );
  },
  //ios设置没有定位跳转到系统设置权限页
  pushSetting: function (onSuccess, onError) {
    if (cordovaFlag) cordova.exec(onSuccess,onError,'SettingPlugin','pushSetting',[])
  },
  //获取app版本号 添加插件  cordova plugin add cordova-plugin-app-version
  getAppVersion: function (success) {   //success成功回调
    if (cordovaFlag)
      cordova.getAppVersion.getVersionNumber().then(success);
  },
  //相机拍照获取图片  添加插件 cordova plugin add cordova-plugin-camera
  //onCameraSuccess成功回调 onCameraError失败回调
  camera:function(callback){
    if (cordovaFlag)
  	var cameraOptions = {
	    quality: 100,
	    destinationType: Camera.DestinationType.DATA_URL,
	    sourceType: Camera.PictureSourceType.CAMERA,
	    allowEdit: true,
	    encodingType: Camera.EncodingType.JPEG,
	    targetWdith: 100,
	    targetHeight: 100,
	    popoverOptions: CameraPopoverOptions,
	    saveToPhotoAlbum: false
    };
    function onCameraSuccess(data){
      callback && callback(data);
    };
    function onCameraError(res){
      alert('Failed because: ' + res);
    };
	  navigator.camera.getPicture(onCameraSuccess, onCameraError, cameraOptions);
  },
  //上传文件 安装cordova plugin add cordova-plugin-file-transfer
  //fileURL上传文件 SERVER上传地址 success成功回调 fail失败回调
  FileTransfer:function(fileURL,SERVER,callback){   
    if (cordovaFlag)
    var options = new FileUploadOptions();
    //上传参数名
    options.fileKey = "file";
    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
    //上传类型
    options.mimeType = "image/png";
    //options.mimeType = "text/plain";

    //上传参数
    var params = {};
    //params.uid = uuid;
    options.params = params;

    function success(data){
      alert("上传成功! Code = " + data.responseCode);
      callback && callback(data);
    };
    function fail(res){
      alert("上传失败! Code = " + res.code)
    };

    var ft = new FileTransfer();
    ft.upload(fileURL, encodeURI(SERVER), success, fail, options);
  },
  //二维码扫码:安装cordova plugin add com.phonegap.plugins.barcodescanner和cordova-plugin-media-capture
  //success成功回调 error失败回调
  //android上如果效果不好 可以改装插件地址为：
  barcodeScanner:function(callback){
    alert(1);
    if (cordovaFlag)
    cordova.plugins.barcodeScanner.scan(
      function(result){
        callback && callback(result.text)
      }, function(error){ 
        alert(error);
    });
  },
  
  



  //启动页设置cordova plugin add cordova-plugin-splashscreen
  //flag 为true时打开启动页 为false时关闭启动页
  splashscreen:function(flag){
    flag? navigator.splashscreen.show() : navigator.splashscreen.hide();
  },
  //获取设备的硬件和软件信息 安装cordova plugin add cordova-plugin-device
  device:function(data){
    let message;
    switch(data){
      case 'cordova': //设备上运行的cordova版本信息
        message=device.cordova;
        break;
      case 'model': //设备的模型或产品的名称
        message=device.model;
        break;
      case 'platform':  //设备的操作系统名称。
        message=device.platform;
        break;
      case 'uuid':  //设备的全局惟一标识符
        message=device.uuid;
        break;
      case 'version': //操作系统版本号
        message=device.version;
        break;
      case 'manufacturer':  //设备的制造商
        message=device.manufacturer;
        break;
      case 'isVirtual': //是否运行在模拟器
        message=device.isVirtual;
        break;
      case 'serial':  //设备硬件序列号
        message=device.serial;
        break;
    };
    return message;
  },
  //微信登录安装插件https://gitee.com/zgw0223/cordova-plugin-wechat.git --variable wechatappid=YOUR_WECHAT_APPID
  //android平台的话需要在plugins\cordova-plugin-wechat\src\android\Wechat.java 602行加上你的appid
    //注（微信登录需要去公众平台配置）签名。签名的获取方式可以参考官方文档上的https://open.weixin.qq.com/cgi-bin/showdocument?action=dir_list&t=resource/res_list&verify=1&id=open1419319167&token=2c4b200fd6dc1cf1450473eead1b617257c2a8f9&lang=zh_CN
  wechatLogin:function(callback){
    //判断是否安装微信
    Wechat.isInstalled(function (installed) {
        // alert("Wechat installed: " + (installed ? "Yes" : "No"));
    }, function (reason) {
        // alert("Failed: " + reason);
    });
    var scope = "snsapi_userinfo",
    state = "_" + (+new Date());
    Wechat.auth(scope, state, function (response) {   //微信登录
        // you may use response.code to get the access token.
        callback && callback(response);
    }, function (reason) {
        alert("Failed: " + reason);
    });
  },
  wechatShare:function(){     //微信分享
    //Share link
    // Wechat.share({
    //   message: {
    //       media: {
    //           type: Wechat.Type.WEBPAGE,
    //           webpageUrl: "http://www.baidu.com"
    //       }
    //   },
    //   scene: Wechat.Scene.TIMELINE   // share to Timeline
    // }, function () {
    //     alert("Success");
    // }, function (reason) {
    //     alert("Failed: " + reason);
    // });
    // Share media(e.g. link, photo, music, video etc)
    Wechat.share({
      message: {
          title: "Hi, there",
          description: "This is description.",
          thumb: "www/img/thumbnail.png",
          mediaTagName: "TEST-TAG-001",
          messageExt: "这是第三方带的测试字段",
          messageAction: "",
          media: ""
      },
      scene: Wechat.Scene.TIMELINE   // share to Timeline
    }, function () {
        alert("Success");
    }, function (reason) {
        alert("Failed: " + reason);
    });
    // Share text

    // Wechat.share({
    //     text: "This is just a plain string",
    //     scene: Wechat.Scene.TIMELINE   // share to Timeline
    // }, function () {
    //     alert("Success");
    // }, function (reason) {
    //     alert("Failed: " + reason);
    // });
  },
  wechatPay:function(params,success,error){     //微信支付
    Wechat.sendPaymentRequest(params,success,error);
  },
  //联系人  安装插件cordova plugin add cordova-plugin-contacts
  contacts:function(callback){
    let contact=[];
    var options = new ContactFindOptions();
    var fields = ["displayName", "phoneNumbers"];
    options.filter = "";
    options.multiple = true;
    options.hasPhoneNumber = true;
    // options.desiredFields = ["displayName", "phoneNumbers"];
  
    navigator.contacts.find(fields, function(contacts) {
      onSuccess(contacts);
  
    }, function onError(contactError) {
      alert(contactError);
    }, options);
    function onSuccess(contacts){
      // alert(JSON.stringify(contacts));
      var contactsLength = contacts.length;
      
      for(var i = 0; i < contactsLength; i++) {
        // console.log('obj for '+i);
        var obj = {};
        obj.name = contacts[i].displayName;
        obj.phoneNumbers = contacts[i].phoneNumbers[0].value;
        contact.push(obj);
        // contact = contact.concat([obj]);
        // _this.$set(contact, i, obj);
      };
      callback && callback(contact)
    }
  }
}
