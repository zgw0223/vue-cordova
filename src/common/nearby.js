// 获得附近地点（百度地图pio信息查询）
function displayPOI(lng,lat,r){//参数：经lat、纬度lng、半径r
	var allPois=[];
	var myGeo = new BMap.Geocoder();
    var mPoint= new BMap.Point(lng,lat);
    var mOption = {
        poiRadius : r,           //半径为r米内的POI,
        numPois : 12             //最多只有12个 系统决定的
    }
    var ponits_=[];//经纬度和地址信息
    myGeo.getLocation(mPoint,function mCallback(rs){
	    allPois = rs.surroundingPois; //获取全部POI(半径R的范围 最多12个点)
	    console.log(allPois);
	     return allPois;
	},mOption);
     console.log(allPois);
};
export{
	displayPOI
}
